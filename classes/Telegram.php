<?php

namespace ASMilano\MailTelegram\Classes;

use ASMilano\MailTelegram\Models\Settings;
use JsonException;

class Telegram extends Gateway
{
    public function __construct(array $options = [])
    {
        $token = $options['telegram_token'] ?? Settings::get('telegram_token');

        $this->name = 'telegram';

        $options += [
            'host' => 'api.telegram.org',
            'port' => 443,
        ];

        parent::__construct($token, $options);

        $this->apiUrl = "{$this->protoPart}://{$this->host}{$this->portPart}/bot{$token}";
    }

    /**
     * @throws JsonException
     */
    public function request(string $method, array $params = []): mixed
    {
        $url = "{$this->apiUrl}/$method";
        $query = http_build_query($params);

        curl_setopt($this->handle, CURLOPT_POST, true);
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, $query);
        curl_setopt($this->handle, CURLOPT_URL, $url);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($this->handle);

        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws JsonException
     */
    public function sendMessage(array $params = []): mixed
    {
        return $this->request('sendMessage', [
            'chat_id' => $params['chat_id'],
            'text' => $params['text'],
            'parse_mode' => $params['parse_mode'] ?? null,
            'reply_markup' => $params['reply_markup'] ?? null,
        ]);
    }
}
