<?php

namespace ASMilano\MailTelegram\Classes;

use CurlHandle;
use Exception;

abstract class Gateway
{
    public string|null $name = null;

    protected string $host;
    protected int $port;
    protected string $apiUrl;

    protected string $protoPart;
    protected string $portPart;

    protected false|CurlHandle $handle;
    protected string $botToken;

    /**
     * @throws Exception
     */
    public function __construct(string $token, array $options = [])
    {
        if ($this->name === null) {
            throw new Exception('Gateway name is empty!');
        }

        $this->handle = curl_init();
        $this->host = $options['host'];
        $this->port = $port = $options['port'];
        $this->botToken = $token;

        $this->protoPart = $port === 443 ? 'https' : 'http';
        $this->portPart = ($port === 443 || $port === 80) ? '' : ':' . $port;
    }

    /**
     * Sending request to bot
     */
    abstract protected function request(string $method, array $params = []): mixed;

    /**
     * Sending message to bot
     */
    abstract public function sendMessage(array $params = []): mixed;
}
