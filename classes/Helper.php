<?php namespace ASMilano\MailTelegram\Classes;

use ASMilano\MailTelegram\Models\Settings;
use Illuminate\Mail\Message;
use JsonException;
use Winter\Storm\Support\Traits\Singleton;

class Helper
{
    use Singleton;

    /**
     * @throws JsonException
     */
    public function send(Message $message): void
    {
        $settings = Settings::instance();
        $dontSendInDebugMode = config('app.debug') === true && $settings->disabled_in_debug === true;

        if ($settings->disabled_sending || $dontSendInDebugMode) {
            return;
        }

        if (!empty($settings->admins_to_send)) {
            foreach ($message->getTo() as $address) {
                if (!in_array($address->getAddress(), $settings->admins_to_send, true)) {
                    return;
                }
            }
        }

        $html = $message->getHtmlBody();

        $text = $this->makeTextFromHTML($html, $settings->strip_eols);

        $telegram = new Telegram();

        if (is_array($settings->telegram_chat_ids)) {
            foreach ($settings->telegram_chat_ids as $telegramChat) {
                $telegram->sendMessage([
                    'chat_id' => $telegramChat['chat_id'],
                    'text' => $text,
                    'parse_mode' => 'HTML',
                ]);
            }
        }
    }

    /**
     * Strip tags, spaces, ends of lines
     */
    public function makeTextFromHTML(string $html, bool $stripEol = false): array|string|null
    {
        $result = [];

        //Remove style tag with its content
        $html = preg_replace('/<style[^>]*>[^<]*<[^>]*>/i', '', $html);

        $text = strip_tags($html);

        if ($stripEol) {
            $text = preg_replace('/\s+/', ' ', $text);
        } else {
            $rows = explode(PHP_EOL, $text);

            foreach ($rows as $row) {
                $row = trim($row);

                if ($row) {
                    $result[] = $row;
                }
            }

            $text = implode(PHP_EOL, $result);
        }

        return $text;
    }
}
