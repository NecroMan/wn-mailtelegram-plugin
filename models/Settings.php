<?php

namespace ASMilano\MailTelegram\Models;

use Backend\Models\User;
use Winter\Storm\Database\Model;

class Settings extends Model
{
    public $implement = [
        'System.Behaviors.SettingsModel',
    ];

    public string $settingsCode = 'mail_telegram_settings';
    public string $settingsFields = 'fields.yaml';

    public function initSettingsData(): void
    {
        $this->disabled_in_debug = false;
        $this->disabled_sending = false;
    }

    public function getAdminsToSendOptions()
    {
        return User::get()->pluck('email', 'email');
    }
}
