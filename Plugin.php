<?php

namespace ASMilano\MailTelegram;

use ASMilano\MailTelegram\Classes\Helper;
use ASMilano\MailTelegram\FormWidgets\BotLog;
use ASMilano\MailTelegram\Models\Settings;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{

    public function pluginDetails(): array
    {
        return [
            'name' => 'vdomah.mailtelegram::lang.plugin.name',
            'description' => 'vdomah.mailtelegram::lang.plugin.description',
            'author' => 'Art Gek / AS Milano',
            'icon' => 'icon-envelope-o',
            'homepage' => 'https://as-milano.ru',
            'replaces' => [
                'Vdomah.MailTelegram' => '<=1.0.12',
            ]
        ];
    }

    public function boot(): void
    {
        Event::listen('mailer.prepareSend', static function ($mailerInstance, $view, $message) {
            Helper::instance()->send($message);

            if (Settings::get('prevent_mail_sending', false)) {
                return false;
            }
        });
    }

    public function registerSettings(): array
    {
        return [
            'settings' => [
                'label' => 'vdomah.mailtelegram::lang.settings.label',
                'description' => 'vdomah.mailtelegram::lang.settings.description',
                'category' => SettingsManager::CATEGORY_NOTIFICATIONS,
                'icon' => 'icon-envelope-o',
                'class' => Settings::class,
                'order' => 500,
                'permissions' => ['vdomah.mailtelegram.access_settings'],
            ],
        ];
    }

    public function registerFormWidgets(): array
    {
        return [
            BotLog::class => [
                'label' => 'Bot Log',
                'code' => 'botlog'
            ],
        ];
    }

    public function registerPermissions(): array
    {
        return [
            'vdomah.mailtelegram.access_settings' => [
                'tab' => 'vdomah.mailtelegram::lang.plugin.name',
                'label' => 'vdomah.mailtelegram::lang.permissions.access_settings'
            ],
        ];
    }
}
