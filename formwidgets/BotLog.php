<?php

namespace ASMilano\MailTelegram\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Exception;

/**
 * Widget to create set of translatable fields grouped into language tabs.
 */
class BotLog extends FormWidgetBase
{
    /**
     * @var array Form field configuration
     */
    public $form;

    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'botlog';

    /**
     * @var array Collection of form widgets.
     */
    protected $formWidgets = [];

    public $tabs = [];

    public $previewMode = false;

    public $viewPathBackend;

    public function init()
    {
        $this->fillFromConfig([
            'form',
        ]);

        $this->bindToController();

        $this->viewPathWidget = 'asmilano/mailtelegram/formwidgets/botlog/partials/';
    }

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial($this->viewPathWidget . 'default');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $updates = [];
        $logs = [];

        try {
            if ($this->model->telegram_token) {
                $resp = file_get_contents('https://api.telegram.org/bot' . $this->model->telegram_token . '/getUpdates');

                if ($resp) {
                    $arResp = json_decode($resp, false, 512, JSON_THROW_ON_ERROR);

                    if ($arResp) {
                        foreach ($arResp->result as $update) {
                            if (!property_exists($update, 'message') || $update->message->from->is_bot) {
                                $updates[] = $update;
                            } elseif ($update->message->chat) {
                                $logs = [
                                    'User or chat ID' => (property_exists($update->message->chat,
                                        'id') ? $update->message->chat->id : ''),
                                    'First name' => (property_exists($update->message->chat,
                                        'first_name') ? $update->message->chat->first_name : ''),
                                    'Last name' => (property_exists($update->message->chat,
                                        'last_name') ? $update->message->chat->last_name : ''),
                                    'Username' => (property_exists($update->message->chat,
                                        'username') ? $update->message->chat->username : ''),
                                    'Date' => date('F j, Y H:i:s', $update->message->date),
                                ];
                            } else {
                                $updates[] = $update->message->chat;
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $updates[] = $e->getMessage();
        }

        $this->vars['model'] = $this->model;
        $this->vars['updates_log'] = $logs;
        $this->vars['updates_arr'] = $updates;
    }
}
